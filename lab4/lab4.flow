import string;
import lingo/pegcode/driver;
import ds/tree;
import grammar;

s2ar (str : string) -> ArExpr {
    e_gr = "#include arith.lingo";
    parsic( 
            compilePegGrammar(e_gr),
            str,
            SemanticActions(setTree(defaultPegActions.t, "createArInt", \s -> ArInt(s2i(s[0]))))
            //defaultPegActions
            )
}

calc_ar (ar : ArExpr, vars : Tree <string, rational>) -> Maybe<rational> {
    switch(ar) {
        ArVar(var) : {
            switch(lookupTree(vars, var)) {
                Some(val) : Some(val);
                None() : {
                    println("Variables not init!");
                    None();
                }
            }
        }
        ArInt(val) : {
            Some(rational(val, 1));
        }
        ArRat(val) : {
            Some(val);
        }
        ArSum(lhs, rhs) : {
            switch(calc_ar(lhs, vars)) {
                Some(l) : {
                    switch(calc_ar(rhs, vars)) {
                        Some(r) : Some(sum_rationals(l, r));
                        None() : None();
                    }
                }
                None() : None();
            }
        }
        ArMult(lhs, rhs) : {
            switch(calc_ar(lhs, vars)) {
                Some(l) : {
                    switch(calc_ar(rhs, vars)) {
                        Some(r) : Some(mult_rationals(l, r));
                        None() : None();
                    }
                }
                None() : None();
            }
        }
        ArSub(lhs, rhs) : {
            switch(calc_ar(lhs, vars)) {
                Some(l) : {
                    switch(calc_ar(rhs, vars)) {
                        Some(r) : Some(sub_rationals(l, r));
                        None() : None();
                    }
                }
                None() : None();
            }
        }
        ArDiv(lhs, rhs) : {
            switch(calc_ar(lhs, vars)) {
                Some(l) : {
                    switch(calc_ar(rhs, vars)) {
                        Some(r) : {
                            if (r.den == 0 || r.num == 0 || l.den == 0) {
                                println("Division by zero!");
                                None();
                            } else {
                                Some(div_rationals(l, r));
                            }
                        }
                        None() : None();
                    }
                }
                None() : None();
            }
        }
        ArMinus(e) : {
            switch(calc_ar(e, vars)) {
                Some(val) : Some(minus_rational(val));
                None() : None();
            }
        }
    }
}

diff (ar : ArExpr, var : string) -> ArExpr {
    switch(ar) {
        ArVar(name) : {
            if (name == var) { 
                ArInt(1);
            } else { 
                ArInt(0);
            }
        }
        ArInt(val) : ArInt(0);
        ArRat(val) : ArInt(0);
        ArSum(lhs, rhs) : ArSum(diff(lhs, var), diff(rhs,var));
        ArMult(lhs, rhs) : ArSum(ArMult(diff(lhs, var), rhs), ArMult(lhs, diff(rhs, var)));
        ArSub(lhs, rhs) : ArSub(diff(lhs, var), diff(rhs,var));
        ArDiv(lhs, rhs) : ArDiv(ArSub(ArMult(diff(lhs, var), rhs), ArMult(lhs, diff(rhs, var))), ArMult(rhs, rhs));
        ArMinus(e) : ArMinus(diff(e, var))
    }
}

main () {
    vars1 : Tree<string, rational> = setTree(makeTree(), "x", rational(3, 4));
    vars2 = setTree(vars1, "y", rational(1, 2));
    vars3 = setTree(vars2, "z", rational(2, 1));
    vars4 = setTree(vars3, "a", rational(8, 9));
    vars5 = setTree(vars4, "b", rational(3, 7));

    //expr = s2ar("(((-x) - ((y + x) / (y * z))) + 1)");
    //expr = s2ar("((x + y) - z)");
    //expr = s2ar("(-((a / y) * b))");
    //expr = s2ar("((x - y) + (x - y))");
    //expr = s2ar("(x / (0 / 23))");
    //expr = s2ar("(1 / (y - y))");
    
    expr = s2ar("(1/x)");

    switch(calc_ar(expr, vars5)) {
        Some(val) : println(r2s(val));
        None() : println("Error!");
    }
    
    diff_expr = simplify(diff(expr, "x"));
    println(ar2s(diff_expr));
}
